package main

import (
	"flag"
	"time"
	"fmt"
	"math/rand"
)

const RIGHT  int = 0
const LEFT  int = 1

type Message struct {
	id int
	index int
	direction int
}

func node(index int, id int, result_chan, my chan Message, neighbour[] chan Message){
	neighbour[LEFT] <- Message{id, index, LEFT}
	neighbour[RIGHT] <- Message{id, index, RIGHT}

	for {
		r := <-my
		//fmt.Printf("index=%d, dir=%d\n",index, r.direction)

		if r.id == id {
			result_chan <- r
			fmt.Println("MAM")
			break
		} else if r.id > id {
			neighbour[r.direction] <- r
		} else {
		// discard
		}

	}

}

func main() {
	var n int
	flag.IntVar(&n, "n", 10, "number of vertices in ring. defaults to 10.")
	flag.Parse()
	fmt.Printf("n = %d\n", n)

	rand.Seed(time.Now().UTC().UnixNano())

	ids := make([]int, n)
	for i, _ := range(ids) {
		ids[i] = rand.Int()
	}
	for i:=0; i<n; i++ {
		fmt.Printf("id[%d] : %d\n", i, ids[i])
	}

	result_chan := make(chan Message)

	node_channels := make([]chan Message, n)
	for i:=0; i<n; i++ {
		node_channels[i] = make(chan Message, 8)
	}

	for i:=0; i<n; i++ {
		right := (i+1)%n
		left := (n-1+i)%(n)
		neighbours := make([]chan Message, 2)
		neighbours[LEFT] = node_channels[left]
		neighbours[RIGHT] = node_channels[right]
		go node(i, ids[i], result_chan, node_channels[i], neighbours)
	}

	m := <- result_chan
	//for i := 0; i < n; i++ {
	//	close(node_channels[i])
	//}
	fmt.Printf("Leader id = %d\n", m)
}